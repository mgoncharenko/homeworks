package bat.operations;

public class Const implements OperationInterface {
    private int value;

    public Const(int value) {
        this.value = value;
    }

    @Override
    public int getValue() {
        return value;
    }

    // just do nothing here
    @Override
    public OperationInterface assignVar(String varName, int varValue) {
        return this;
    }
}