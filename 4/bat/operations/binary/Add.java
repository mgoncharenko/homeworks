package bat.operations.binary;

import bat.operations.OperationInterface;

public class Add extends BinaryOperation {

    public Add(OperationInterface v1, OperationInterface v2) {
        super(v1, v2);
    }

    @Override
    public int getValue() {
        return v1.getValue() + v2.getValue();
    }
}