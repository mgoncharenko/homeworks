package bat.operations.binary;

import bat.operations.OperationInterface;

abstract public class BinaryOperation implements OperationInterface {
    protected OperationInterface v1;
    protected OperationInterface v2;

    @Override
    public BinaryOperation assignVar(String varName, int varValue) {
        v1.assignVar(varName, varValue);
        v2.assignVar(varName, varValue);

        return this;
    }

    BinaryOperation(OperationInterface v1, OperationInterface v2) {
        this.v1 = v1;
        this.v2 = v2;
    }







}
