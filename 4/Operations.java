
import bat.operations.Const;
import bat.operations.OperationInterface;
import bat.operations.Variable;
import bat.operations.binary.Add;
import bat.operations.binary.Multiply;
import bat.operations.binary.Subtract;



public class Operations {










	public static void main(String[] args) {
		if(args.length == 0) {
			System.out.println("Please specify X");
		}
		else {


		    int x = Integer.parseInt(args[0]);

			OperationInterface op =
					new Add(
							new Subtract(
							        new Multiply(new Variable("x"), new Variable("x")),
									new Multiply(new Const(2), new Variable("x"))
									),
	                    new Const(1)
	                ).assignVar("x", x);

			System.out.println(op.getValue());
		}
	}



}
