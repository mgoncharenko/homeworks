
public class Operations {

	interface OperationInterface {
		public OperationInterface assignVar(String varName, int varValue);
		public int getValue();
	}


	private static class Const implements OperationInterface {
		private int value;

		public Const(int value) {
			this.value = value;
		}

		@Override
		public int getValue() {
			return value;
		}

		// just do nothing here
		@Override
		public OperationInterface assignVar(String varName, int varValue) { return this; }
	}

	private static class Variable implements OperationInterface {
		private String varName;
		private int value;

		public Variable(String varName) {
			this.varName = varName;
		}

		@Override
		public int getValue() {
			return value;
		}

		@Override
		public OperationInterface assignVar(String varName, int varValue) {
			if(varName == this.varName) {
				this.value = varValue;
			}

			return this;
		}
	}

	abstract private static class BinaryOperation implements OperationInterface {
		protected OperationInterface v1;
		protected OperationInterface v2;

		@Override
		public BinaryOperation assignVar(String varName, int varValue) {
			v1.assignVar(varName, varValue);
			v2.assignVar(varName, varValue);

			return this;
		}

		BinaryOperation(OperationInterface v1, OperationInterface v2) {
			this.v1 = v1;
			this.v2 = v2;
		}
	}

	private static class Add extends BinaryOperation {

		Add(OperationInterface v1, OperationInterface v2) {
			super(v1, v2);
		}

		@Override
		public int getValue() {
			return v1.getValue() + v2.getValue();
		}
	}

	private static class Subtract extends BinaryOperation {

		Subtract(OperationInterface v1, OperationInterface v2) {
			super(v1, v2);
		}

		@Override
		public int getValue() {
			return v1.getValue() - v2.getValue();
		}
	}


	private static class Multiply extends BinaryOperation {

		Multiply(OperationInterface v1, OperationInterface v2) {
			super(v1, v2);
		}

		@Override
		public int getValue() {
			return v1.getValue() * v2.getValue();
		}
	}

	private static class Divide extends BinaryOperation {

		Divide(OperationInterface v1, OperationInterface v2) {
			super(v1, v2);
		}

		@Override
		public int getValue() {
			return v1.getValue() / v2.getValue();
		}
	}


	public static void main(String[] args) {
		if(args.length == 0) {
			System.out.println("Please specify X");
		}
		else {


		    int x = Integer.parseInt(args[0]);

			OperationInterface op =
					new Add(
							new Subtract(
							        new Multiply(new Variable("x"), new Variable("x")),
									new Multiply(new Const(2), new Variable("x"))
									),
	                    new Const(1)
	                ).assignVar("x", x);

			System.out.println(op.getValue());
		}
	}



}
