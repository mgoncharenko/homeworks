package bat;

import bat.task.ITask;

public class __ThreadRunner<X, Y> implements Runnable {

    private ITask<X, Y> task = null;
    private X result = null;
    private Y input = null;

    public __ThreadRunner(ITask<X, Y> task, Y input) {
        this.task = task;
        this.input = input;
    }

    @Override
    public void run() {
        result = task.run(input);
    }

    public X getResult() {
        return this.result;
    }
}
