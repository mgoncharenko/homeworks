package bat.task;


public interface ITask<X, Y> {

    X run(Y value);

}