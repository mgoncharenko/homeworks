package bat.task;

public class StrLen extends Abstract<Integer, String> implements ITask<Integer, String> {

    @Override
    public Integer run(String value) {
        try {
            Thread.sleep(200);
        }
        catch(InterruptedException e) {
        }
        return value.length();
    }

}
