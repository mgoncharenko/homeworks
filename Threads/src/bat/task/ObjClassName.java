package bat.task;

public class ObjClassName extends Abstract<String, Object> implements ITask<String, Object> {

    @Override
    public String run(Object value) {

        try {
            Thread.sleep(200);
        }
        catch(InterruptedException e) {
        }


        return "[" + value.getClass().getName() + "] " + value;
    }

}
