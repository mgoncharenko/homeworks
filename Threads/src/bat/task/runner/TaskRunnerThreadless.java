package bat.task.runner;

import bat.task.ITask;

public class TaskRunnerThreadless extends TaskRunnerAbstract implements ITaskRunner {


    @Override
    public <X, Y> X run(ITask<X, Y> task, Y value) {

        return task.run(value);
    }


}
