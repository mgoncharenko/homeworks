package bat.task.runner;

import java.util.Stack;

import bat.Main;
import bat.task.ITask;

public class TaskRunnerImpl extends TaskRunnerAbstract implements ITaskRunner {

    private final int THREAD_COUNT = 2;
    private Thread [] threads = new Thread[THREAD_COUNT];

    private int activeThreadCount = 0;

    private Monitor monitor = new Monitor();

    private class TaskProcessor<X, Y> implements Runnable {

        private ITask<X, Y> task;
        private Y value;
        private X result;

        public TaskProcessor(ITask<X, Y> task, Y value) {
            this.task = task;
            this.value = value;
        }

        @Override
        public void run() {
            result = task.run(value);
        }

        public X getResult() {
            return result;
        }
    }

    private class Monitor {

        private Stack<Integer> freeThreads;

        public Monitor() {
            freeThreads = new Stack<Integer>();
        }

        public void pushThreadNo(int threadNo) {
            freeThreads.push(threadNo);
        }

        public boolean hasThreads() {
            return (freeThreads.size() > 0);
        }

        public int popThreadNo() {
            return freeThreads.pop();
        }
    }


    @Override
    public <X, Y> X run(ITask<X, Y> task, Y value) {

        TaskProcessor<X, Y> tp = new TaskProcessor<X, Y>(task, value);
        Thread threadToLaunch = null;
        int threadNo = -1;
        boolean newThreadCreated = false;

        synchronized(monitor) {
            if(activeThreadCount < THREAD_COUNT) {
                Main.log("Creating a NEW thread for task: [" + value + "]. Active threads: " + activeThreadCount);

                threadToLaunch = new Thread(tp);
                threads[activeThreadCount] = threadToLaunch;
                threadNo = activeThreadCount;
                newThreadCreated = true;
                activeThreadCount++;
            }
        }

        if(!newThreadCreated) {
            try {
                Main.log("Waiting for a free thread for task: [" + value + "]");
                synchronized(monitor) {

                    // wait only if there's no threads in the queue. Pick up the first available thread otherwise
                    while(!monitor.hasThreads()) {
                        monitor.wait();
                    }

                    threadNo = monitor.popThreadNo();

                    threadToLaunch = new Thread(tp);
                    threads[threadNo] = threadToLaunch;
                    Main.log("Thread is freed for task: [" + value + "]: " + threadNo);
                }

            } catch (InterruptedException e) {
                return null;
            }
        }

        threadToLaunch.start();

        try {
            threadToLaunch.join();

        } catch (InterruptedException e) {
            Main.log("Thread interrupted!?");
        }

        synchronized(monitor) {
            monitor.pushThreadNo(threadNo);
            monitor.notify();
        }

        return tp.getResult();
    }


}
