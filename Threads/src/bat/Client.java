package bat;

import java.util.ArrayList;

import bat.task.ITask;
import bat.task.runner.ITaskRunner;

public class Client implements Runnable {

    private static int clientIdCounter = 0;

    private ITaskRunner runner;

    private int step = 0;
    private int clientId = 0; // if used by several clients, every should have its own number

    public Client() {
        clientIdCounter++;
        this.clientId = clientIdCounter;
    }

    public ITask<String, Object> generateTask() {
        return TaskGenerator.createObjClassName();
    }

    public void setRunner(ITaskRunner runner) {
        this.runner = runner;
    }

    public void loop5IterationsInANewThread(ArrayList<Thread> threadList) {

        int i;

        for(i = 0; i < 5; i++) {
            Thread th = new Thread(this);
            threadList.add(th);
            th.start();
            //th.join();
        }
    }



    public void loopIteration() {

        String statusStr;
        ITask<String, Object> task;

        synchronized (this) {
            step++;
            statusStr = "Client " + clientId + "/" + clientIdCounter + " running step " + step;
            task = generateTask();
        }

        String result = runner.run(task, statusStr);
        Main.log("Finished: " + result);

        //Main.log(statusStr);
        //Main.log(result);

    }

    @Override
    public void run() {
        this.loopIteration();
    }



















}
