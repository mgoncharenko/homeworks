package bat;

import java.util.ArrayList;

import bat.task.runner.ITaskRunner;
import bat.task.runner.TaskRunnerImpl;


public class Main {

    public static void log(String str) {
        System.out.println(str);
    }



    public static void main(String[] args) {
        log("Started");

        int CLIENT_COUNT = 10;
        int i;

        ArrayList<Thread> threads = new ArrayList<Thread>();

        Client[] clients = new Client[CLIENT_COUNT];
        ITaskRunner taskRunner = new TaskRunnerImpl();
        for(i = 0; i < CLIENT_COUNT; i++) {
            clients[i] = new Client();
            clients[i].setRunner(taskRunner);
        }

        for(i = 0; i < CLIENT_COUNT; i++) {
            clients[i].loop5IterationsInANewThread(threads);
        }

        log("main() waiting for finish");
        for(i = 0; i < threads.size(); i++) {
            try {
                threads.get(i).join();
            }
            catch(InterruptedException e) {
            }
        }

        log("main() Finished");
    }
}
