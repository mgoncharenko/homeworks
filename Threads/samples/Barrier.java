package threads;

public final class Barrier {
    private int generation;

    public void await(Barrier b) throws InterruptedException { // 0
        synchronized (this) { // 1
            generation++;     // 2
            this.notify();    // 3
        } // 4
        synchronized (b) { // 5
            while (generation > b.generation) { // 6
                b.wait(); // 7
            } // 8
        } // 9
    }    
}
