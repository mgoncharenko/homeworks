package threads;

public final class BarrierMain implements Runnable {
    private final String name;
    private final Barrier b1;
    private final Barrier b2;

    public BarrierMain(String name, Barrier b1, Barrier b2) {
        this.name = name;
        this.b1 = b1;
        this.b2 = b2;
    }

    public void run() {
        try {
            for (int i = 0; i < 10; i++) {
                b1.await(b2);
                System.out.format("%s: %d\n", name, i);
            }
        } catch (InterruptedException e) {
            System.out.println("Interrupted");
        }
    }

    public static void main(String[] args) {
        final Barrier b1 = new Barrier();
        final Barrier b2 = new Barrier();
        
        new Thread(new BarrierMain("1", b1, b2)).start();
        new Thread(new BarrierMain("2", b2, b1)).start();
    }
}
