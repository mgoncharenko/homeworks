package threads;

import java.util.concurrent.locks.*;

public final class DeadlockConcurrent implements Runnable {
    private final Lock l1;
    private final Lock l2;
    private final Condition c1;
    private final Condition c2;

    public DeadlockConcurrent(Lock l1, Lock l2, Condition c1, Condition c2) {
        this.l1 = l1;
        this.l2 = l2;
        this.c1 = c1;
        this.c2 = c2;
    }

    public void run() {
        try {
            // 0
            l1.lock();
                // 1
                c1.signal();
                // 2
                l2.lock();
                    // 3
                    l2.unlock();
                    // 4
                    c2.await();
                    // 5
                    l2.lock();
                    // 6
                l2.unlock();
                // 7
            l1.unlock();
            // 8
        } catch (InterruptedException e) {
            System.out.println("Interrupted");
        }
    }

    public static void main(String[] args) {
        Lock l1 = new ReentrantLock();
        Lock l2 = new ReentrantLock();
        Condition c1 = l1.newCondition();
        Condition c2 = l1.newCondition();
        new Thread(new DeadlockConcurrent(l1, l2, c1, c2)).start();
        new Thread(new DeadlockConcurrent(l2, l1, c2, c1)).start();
    }
}
