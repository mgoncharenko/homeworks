package threads;

public final class Deadlock implements Runnable {
    private final Object o1;
    private final Object o2;

    public Deadlock(Object o1, Object o2) {
        this.o1 = o1;
        this.o2 = o2;
    }

    public void run() {
        synchronized (o1) {
            o1.notifyAll();
            synchronized (o2) {
                try {
                    o2.wait();
                } catch (InterruptedException e) {}
            }
        }
    }

    public void run2() {
        synchronized (o2) {
            o2.notifyAll();
            synchronized (o1) {
                try {
                    o1.wait();
                } catch (InterruptedException e) {}
            }
        }
    }

    public static void main(String[] args) {
        Object o1 = new Object();
        Object o2 = new Object();
        new Thread(new Deadlock(o1, o2)).start();
        new Thread(new Deadlock(o2, o1)).start();
    }
}
