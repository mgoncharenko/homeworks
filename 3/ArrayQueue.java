
public class ArrayQueue {
	
	private Object[] elements = new Object[0];	
	private int size = 0;	
	
	
	private void ensureCapacity(int requestedCapacity) {		
		int newSize = requestedCapacity + 3;
		
		if(elements.length < requestedCapacity) {
			Object[] newElements = new Object[newSize];
			
			for(int i = 0; i < size; i++) {
				newElements[i] = elements[i];
			}
			
			//size = requestedCapacity;
			elements = newElements;
		}
	}
	
	public int size() {
		return size;
	}	
	
	public void push(Object value) {
		ensureCapacity(size + 1);		
		elements[size] = value;
		size++;		
	}
	
	public Object pop() {		
		if(size <= 0) {
			return null;
		}
		
		size--;
		return elements[size];
	}
	
	
	public Object peek() {
		if(size <= 0) {
			return null;
		}
		
		return elements[size - 1];		
	}
	
	
	

}
