import java.util.Collection;




public class BagTest  {


    public static void main(String[] args) {

        Collection<Integer> bag = new Bag<Integer>();

        bag.add(1);
        bag.add(2);
        bag.add(3);
        bag.add(1);
        bag.add(2);
        bag.add(3);

        /*
        bag.add(1);
        bag.add(4);
        bag.add(2);
        bag.add(3);
        bag.add(1);
        bag.add(3);
        bag.add(1);
        bag.add(2);
        bag.add(2);
        bag.add(2);
        */

        for(Integer i : bag) {
            System.out.print(String.valueOf(i) + "  ");
        }
    }
}
