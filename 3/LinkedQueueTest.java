
public class LinkedQueueTest {
	
	
	public static void main(String[] args) {
		
		LinkedQueue<String> queue = new LinkedQueue<String>();
		
		for(int i = 0; i < 5; i++) {
			queue.push("e" + i);
		}
		
		while(queue.size() > 0) {
			System.out.println(queue.peek() + " - " + queue.pop());
		}
		
	}
	
		

}
