import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import java.util.NoSuchElementException;


public class Bag<E> implements Collection<E> {
	private HashMap<E, ArrayList<E>> elements = new HashMap<E, ArrayList<E>>();
	private int size = 0;

	@Override
    public boolean add(E e) {

	    ArrayList<E> elementList;
	    if(elements.containsKey(e)) {
	        elementList = elements.get(e);
	        elementList.add(e);
	    }
	    else {
	        elementList = new ArrayList<E>();
	        elementList.add(e);
	        elements.put(e, elementList);
	    }

	    size++;

	    return true;
	}


    @Override
    public boolean addAll(Collection<? extends E> arg0) {
        // TODO Auto-generated method stub
        return false;
    }

    @Override
    public void clear() {
        elements.clear();
        size = 0;
    }

    @Override
    public boolean contains(Object key) {
        return elements.containsKey(key);
    }

    @Override
    public boolean containsAll(Collection<?> arg0) {
        // TODO Auto-generated method stub
        return false;
    }

    @Override
    public boolean isEmpty() {
        return elements.isEmpty();
    }

    @Override
    public Iterator<E> iterator() {
        return new BagIterator();
    }

    @Override
    public boolean remove(Object e) {

        if(elements.containsKey(e)) {
            ArrayList<E> al = elements.get(e);
            al.remove(0);
            if(al.size() == 0) {
                elements.remove(e);
            }
            size--;
            return true;
        }
        else {
            return false;
        }
    }

    @Override
    public boolean removeAll(Collection<?> arg0) {
        // TODO Auto-generated method stub
        return false;
    }

    @Override
    public boolean retainAll(Collection<?> arg0) {
        // TODO Auto-generated method stub
        return false;
    }

    @Override
    public int size() {
        // TODO Auto-generated method stub
        return 0;
    }

    @Override
    public Object[] toArray() {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public <T> T[] toArray(T[] arg0) {
        // TODO Auto-generated method stub
        return null;
    }




    private class BagIterator implements Iterator<E> {
        private ArrayList<E> currentList;
        private Iterator<ArrayList<E>> elementsIterator = null;
        private Iterator<E> currentListIterator = null;


        public BagIterator() {
            elementsIterator = elements.values().iterator();
            jumpToNextList();
            //currentList = elementsIterator.next();
            //currentListIterator = currentList.iterator();
        }


        private void jumpToNextList() {
            currentList = elementsIterator.next();
            currentListIterator = currentList.iterator();
        }

        @Override
        public boolean hasNext() {
            if(!currentListIterator.hasNext()) {
                return elementsIterator.hasNext();
            }
            else return true;
        }

        @Override
        public E next() {
            if(!currentListIterator.hasNext()) {
                if(!elementsIterator.hasNext()) {
                    throw new NoSuchElementException();
                }
                jumpToNextList();
            }

            return currentListIterator.next();
        }

    }








}
