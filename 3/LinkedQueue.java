
public class LinkedQueue<E> {

	int size = 0;
	Node<E> head = null;
		
	private class Node<T> {
		private Node<T> next;
		private T value;
		
		private Node(T value, Node<T> next) {
			this.value = value;
			this.next  = next;
		}		
	}	
	
	public int size() {
		return size;
	}	
	
	public void push(E value) {		
		head = new Node<E>(value, head);
		size++;
	}
	
	public E pop() {		
		if(size <= 0) {
			return null;
		}
		
		Node<E> r = head;
		head = head.next;
		size--;
		return r.value;
	}
	
	
	public E peek() {
		return head.value;
	}
	
	
	

}
