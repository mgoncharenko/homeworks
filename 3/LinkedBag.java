import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import java.util.NoSuchElementException;


public class LinkedBag<E> implements Collection<E> {
	private HashMap<E, ArrayList<Node>> elements = new HashMap<E, ArrayList<Node>>();

	private Node firstNode = null;
	private Node lastNode = null;

	private int size = 0;

	private class Node {
	    private E element;
	    private Node next;

	    public Node(E e) {
	        element = e;
	    }

	    public E getElement() {
	        return element;
	    }

	    public void setNext(Node node) {
	        next = node;
	    }

	    public Node getNext() {
	        return next;
	    }
	}

	@Override
    public boolean add(E e) {

	    Node newNode = new Node(e);
	    if(lastNode != null) {
	        lastNode.setNext(newNode);
	    }

	    if(firstNode == null) {
	        firstNode = newNode;
	    }

	    lastNode = newNode;

	    ArrayList<Node> elementList;
	    if(elements.containsKey(e)) {
	        elementList = elements.get(e);
	        elementList.add(newNode);
	    }
	    else {
	        elementList = new ArrayList<Node>();
	        elementList.add(newNode);
	        elements.put(e, elementList);
	    }

	    size++;
	    return true;
	}


    @Override
    public boolean addAll(Collection<? extends E> arg0) {
        // TODO Auto-generated method stub
        return false;
    }

    @Override
    public void clear() {
        elements.clear();
        lastNode = null;
        size = 0;
    }

    @Override
    public boolean contains(Object key) {
        return elements.containsKey(key);
    }

    @Override
    public boolean containsAll(Collection<?> arg0) {
        // TODO Auto-generated method stub
        return false;
    }

    @Override
    public boolean isEmpty() {
        return elements.isEmpty();
    }

    @Override
    public Iterator<E> iterator() {
        return new LinkedBagIterator();
    }

    @Override
    public boolean remove(Object e) {

        if(elements.containsKey(e)) {
            ArrayList<Node> al = elements.get(e);
            Node nodeToRemove = al.get(0);
            al.remove(0);
            if(al.size() == 0) {
                elements.remove(e);
            }

            // removing first node
            if(nodeToRemove == firstNode) {
                firstNode = firstNode.getNext();
            }
            else {  // removing not first node
                for(Node currentNode = firstNode; currentNode != null; currentNode = currentNode.getNext()) {
                    if(currentNode.getNext() == nodeToRemove) {
                        currentNode.setNext(currentNode.getNext().getNext());
                        break;
                    }
                }
            }

            size--;
            return true;
        }
        else {
            return false;
        }
    }

    @Override
    public boolean removeAll(Collection<?> arg0) {
        // TODO Auto-generated method stub
        return false;
    }

    @Override
    public boolean retainAll(Collection<?> arg0) {
        // TODO Auto-generated method stub
        return false;
    }

    @Override
    public int size() {
        // TODO Auto-generated method stub
        return 0;
    }

    @Override
    public Object[] toArray() {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public <T> T[] toArray(T[] arg0) {
        // TODO Auto-generated method stub
        return null;
    }




    private class LinkedBagIterator implements Iterator<E> {

        private Node currentNode;

        public LinkedBagIterator() {
            currentNode = firstNode;
        }

        @Override
        public boolean hasNext() {
            return (currentNode != null);
        }

        @Override
        public E next() {

            if(currentNode == null) {
                throw new NoSuchElementException();
            }

            E r = currentNode.getElement();
            currentNode = currentNode.getNext();

            return r;
        }
    }








}
