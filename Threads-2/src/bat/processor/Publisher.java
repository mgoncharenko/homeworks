package bat.processor;

import bat.Main;

public class Publisher extends AbstractProcessor {

    private InterProcessorQueue<Integer> inQueue;

    public Publisher(int orderNo, InterProcessorQueue<Integer> inQueue) {
    	super(orderNo);
    	TAG = "Publisher";
        this.inQueue = inQueue;
    }

    @Override
    public void run() {
    	
    	try{    	
	    	while(!thread.isInterrupted()) {    		
	    		while(inQueue.isEmpty() && !thread.isInterrupted()) {
	    			synchronized(inQueue.inMonitor) {
	    				inQueue.inMonitor.wait();
	    			}
	    		}
	    		
	    		int val = inQueue.pop();	    		
	    			
	    		synchronized(inQueue.outMonitor) {	    			
	    			inQueue.outMonitor.notify();
	    		}
	    		
	    		Thread.sleep(250);
	    		Main.log("Published: " + val);
	    	}
    	}
    	catch(InterruptedException e) {
    		Main.log("Interrupted exception");
    	}
    }
}
