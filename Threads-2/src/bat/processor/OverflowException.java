package bat.processor;


public class OverflowException extends Exception {

    /**
     * It is required for some reason.
     */
    private static final long serialVersionUID = 1L;

    public OverflowException(String string) {
        super(string);
    }
}