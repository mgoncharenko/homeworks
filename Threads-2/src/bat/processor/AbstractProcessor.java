package bat.processor;

import bat.Main;

public abstract class AbstractProcessor implements Runnable {

    protected Thread thread = null;
    protected String TAG;
    protected int orderNo;
    
    public AbstractProcessor(int orderNo) {
    	this.orderNo = orderNo;    	
    }

    public void start() {    	
        this.thread = new Thread(this, TAG + "-" + orderNo);
        this.thread.start();
    }
    
    public void terminate() {    	
    	Main.log("Terminating " + this.thread.getName());
    	this.thread.interrupt();    	
    }
    
    public Thread getThread() {
    	return this.thread;
    }
    




}
