package bat.processor;

import bat.Main;
import bat.task.ITask;
import bat.task.StrLen;

public class Worker extends AbstractProcessor {

	private InterProcessorQueue<ITask<?, ?>> inQueue;
	private InterProcessorQueue<Integer> outQueue;

	public Worker(int orderNo, InterProcessorQueue<ITask<?, ?>> inQueue, InterProcessorQueue<Integer> outQueue) {
		super(orderNo);
		TAG = "Worker";
		this.inQueue = inQueue;
		this.outQueue = outQueue;
	}

	@Override
	public void run() {		
		StrLen task;
		
		try {
			while(!thread.isInterrupted()) {
				
				synchronized(inQueue.inMonitor) {
					while(inQueue.isEmpty() && !thread.isInterrupted()) {					
						inQueue.inMonitor.wait();
					}
					
					task = (StrLen)inQueue.pop();
				}
				
				synchronized(inQueue.outMonitor) {
					inQueue.outMonitor.notify();
				}
				
				int r = task.run("Some string");
				
				if(thread.isInterrupted()) {
					return;
				}				
									
				synchronized(outQueue.outMonitor) {
					while(outQueue.isFull() && !thread.isInterrupted()) {
						outQueue.outMonitor.wait();							
					}
					
					outQueue.push(r);
				}				
				
				synchronized(outQueue.inMonitor) {
					outQueue.inMonitor.notify();
				}
			}
		}
		catch(OverflowException e) {
			Main.log("This must not happen!");
		}
		catch(InterruptedException e) {
			Main.log("Interrupted exception");			
		}

	}

}
