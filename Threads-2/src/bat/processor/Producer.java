package bat.processor;

import bat.Main;
import bat.task.ITask;
import bat.task.StrLen;


public class Producer extends AbstractProcessor {
	
    private InterProcessorQueue<ITask<?, ?>> outQueue;
    

    public Producer(int orderNo, InterProcessorQueue<ITask<?, ?>> outQueue) {
    	super(orderNo);
    	TAG = "Producer";
        this.outQueue = outQueue;
    }

    private StrLen generateTask() {
        try {
            Thread.sleep(250);
        } catch (InterruptedException e) {
        }
        return new StrLen();
    }

    @Override
    public void run() {

        StrLen task;
        try {
        	while(!thread.isInterrupted()) {            	
                task = generateTask();
                                
                synchronized(outQueue.outMonitor) {
                    while(outQueue.isFull() && !thread.isInterrupted()) {
                        outQueue.outMonitor.wait();
                    }
                    
                    if(thread.isInterrupted()) {
                    	return;
                    }
                    outQueue.push(task);
                }                
                
                synchronized(outQueue.inMonitor) {
                	outQueue.inMonitor.notify();
                }
            
            }
        
        }
        catch(OverflowException e) {
            // SHOULD NEVER GET HERE
            Main.log("Unbelievable!!! Programmer error!");
        }
        catch(InterruptedException e) {
        	Main.log("Interrupted exception");
        }
    }








}
