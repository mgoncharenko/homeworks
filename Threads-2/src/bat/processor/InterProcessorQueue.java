package bat.processor;

import java.util.LinkedList;

import bat.Main;





public class InterProcessorQueue<T> {

    private LinkedList<T> taskQueue = new LinkedList<T>();

    // wait/notify when anything comes into the queue
    public Object inMonitor = new Object();
    // wait/notify when anything is removed from the queue
    public Object outMonitor = new Object();

    private final int CAPACITY = 3;

    synchronized public boolean isFull() {
        return (taskQueue.size() >= CAPACITY);
    }
    
    synchronized public boolean isEmpty() {
    	return (taskQueue.size() == 0);
    }
    

    synchronized public void push(T value) throws OverflowException {
        if(isFull()) {
            throw new OverflowException("Queue is full");
        }
        
        Main.log("Adding " + value.getClass().getName());
        taskQueue.add(value);
    }

    synchronized public T pop() {
    	
    	T value = taskQueue.removeFirst();    	
    	Main.log("Removing " + value.getClass().getName());    			
        return value;
    }

    synchronized public int size() {
        return taskQueue.size();
    }

    synchronized public boolean hasElements() {
        return (this.size() > 0);
    }
}