package bat.task;

public class StrLen implements ITask<Integer, String> {

    @Override
    public Integer run(String value) {

        try {
			Thread.sleep(1000);
		} catch (InterruptedException e) {
			
			// interrupted flag resets when the exception is thrown. Lets interrupt it once more			
			Thread.currentThread().interrupt();
		}
        return value.length();
    }

}
