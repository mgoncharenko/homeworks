package bat.task.runner;

import bat.task.ITask;


public interface ITaskRunner {
    <X, Y> X run(ITask<X, Y> task, Y value);
}

