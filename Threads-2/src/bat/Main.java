package bat;




public class Main {

    public static void log(String str) {    	
    	String threadName = Thread.currentThread().getName();
        System.out.println(threadName + ": " + str);
    }


    public static void main(String[] args) {
    	
    	Manager m = new Manager();
    	m.init();
    	
    	try {
			Thread.sleep(10000);
		} catch (InterruptedException e) {
			
		}
    	
    	m.terminate();
    	
    	m.join();
    }

}
