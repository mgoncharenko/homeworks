package bat;

import bat.processor.InterProcessorQueue;
import bat.processor.Producer;
import bat.processor.Publisher;
import bat.processor.Worker;
import bat.task.ITask;

public class Manager {

    private final int CAPACITY = 10;

    private Producer[]  producers  = new Producer[CAPACITY];
    private Worker[]    workers    = new Worker[CAPACITY];
    private Publisher[] publishers = new Publisher[CAPACITY];

    public void init() {
        InterProcessorQueue<ITask<?,?>> producerToWorker = new InterProcessorQueue<ITask<?,?>>();
        InterProcessorQueue<Integer> workerToPublisher = new InterProcessorQueue<Integer>();
        int i;

        for(i = 0; i < CAPACITY; i++) {
            producers[i] = new Producer(i, producerToWorker);
            workers[i] = new Worker(i, producerToWorker, workerToPublisher);
            publishers[i] = new Publisher(i, workerToPublisher);
        }
        
        for(i = 0; i < CAPACITY; i++) {
        	producers[i].start();
        	workers[i].start();
        	publishers[i].start();
        }
    }
    
    
    public void terminate() {
    	int i;
    	
    	for(i = 0; i < CAPACITY; i++) {    		
    		producers[i].terminate();
    		workers[i].terminate();
    		publishers[i].terminate();    		
    	}
    }
    
    public void join() {
    	
    	int i;
    	
    	for(i = 0; i < CAPACITY; i++) {
    		Thread t;
    		
    		t = producers[i].getThread();
    		if(t.isAlive()) {
				try {
					t.join();
				} catch (InterruptedException e) {			
				}
    		}
    		
    		t = workers[i].getThread();
    		if(t.isAlive()) {
				try {
					t.join();
				} catch (InterruptedException e) {			
				}
    		}
    		
    		t = publishers[i].getThread();
    		if(t.isAlive()) {
				try {
					t.join();
				} catch (InterruptedException e) {			
				}
    		}
    	}
    }
    
    
    












}