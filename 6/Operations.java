
import java.util.ArrayList;

import bat.operations.OperationInterface;
import bat.operations.binary.BinaryOperation;
import bat.parser.Lexer;
import bat.parser.Lexer.Token;
import bat.parser.Parser;
import bat.parser.Simplifier;



public class Operations {

	public static void main(String[] args) {

	    //String str = "-10 - (20 - (30 - 15)) + 40 - X + 50";

	    //String str = "(1 + (2 - (3 + 4) * 5)) - 6";
	    //String str = "1 + (2 + 3) * (4 + 5) + 6";

	    String str = "1 + (2 + 0) * (4 + 5) + X / X - X + (6 * (0 + 0 / 0 + 0 + 0 + 0 + 0 * 0)) + (X - X) * (Y - Y)";

	    ArrayList<Token> tokens = Lexer.lex(str);
	    System.out.println("====== TOKENS ========");

        for(int i = 0; i < tokens.size(); i++)
        {
            Token token = tokens.get(i);
            System.out.println(i + ": " + token);
        }

        System.out.println("====== PARSE ========");

        Parser parser = new Parser();
        try {
            OperationInterface oi = parser.parse(tokens);

            ((BinaryOperation)oi).detectBrackets();
            System.out.println("Equation           : " + oi.toString());

            Simplifier s = Simplifier.factory((BinaryOperation) oi);
            oi = s.simplify();
            ((BinaryOperation)oi).detectBrackets();

            System.out.println("Equation simplified: " + oi.toString());

            System.out.println("========= PARSED! ===============");


        } catch (Exception e) {

            System.out.println("=========== EXCEPTION ===============");

            e.printStackTrace();
        }



/*
		if(args.length == 0) {
			System.out.println("Please specify X");
		}
		else {
		    int x = Integer.parseInt(args[0]);

			OperationInterface op =
					new Add(
							new Subtract(
							        new Multiply(new Variable("x"), new Variable("x")),
									new Multiply(new Const(2), new Variable("x"))
									),
	                    new Const(1)
	                ).assignVar("x", x);

			System.out.println(op.getValue());
		}
*/


	}
}
