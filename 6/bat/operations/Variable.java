package bat.operations;

public class Variable implements OperationInterface {
    private String varName;
    private int value;

    public Variable(String varName) {
        this.varName = varName;
    }

    @Override
    public int getValue() {
        return value;
    }

    @Override
    public OperationInterface assignVar(String varName, int varValue) {
        if (varName == this.varName) {
            this.value = varValue;
        }

        return this;
    }

    @Override
    public String toString() {
        return varName;
    }

    public Object getVarName() {
        return varName;
    }

}