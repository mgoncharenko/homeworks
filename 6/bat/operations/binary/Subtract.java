package bat.operations.binary;

import bat.operations.OperationInterface;
import bat.parser.Lexer.TokenPriority;

public class Subtract extends BinaryOperation {

    public Subtract(OperationInterface v1, OperationInterface v2) {
        super(v1, v2);
        priority = TokenPriority.ADD;
    }

    @Override
    protected String getSign() {
        return "-";
    }


    @Override
    public int getValue() {
        return v1.getValue() - v2.getValue();
    }
}