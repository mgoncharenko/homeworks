package bat.operations.binary;

import bat.operations.OperationInterface;
import bat.parser.Lexer;
import bat.parser.Lexer.TokenPriority;

abstract public class BinaryOperation implements OperationInterface {
    protected OperationInterface v1;
    protected OperationInterface v2;

    protected Lexer.TokenPriority priority;

    protected boolean bracketsRequired = false;


    @Override
    public OperationInterface assignVar(String varName, int varValue) {
        v1.assignVar(varName, varValue);
        v2.assignVar(varName, varValue);

        return this;
    }

    BinaryOperation(OperationInterface v1, OperationInterface v2) {
        this.v1 = v1;
        this.v2 = v2;
    }

    public OperationInterface getV1() {
        return v1;
    }

    public OperationInterface getV2() {
        return v2;
    }

    public void setV1(OperationInterface v) {
        this.v1 = v;
    }

    public void setV2(OperationInterface v) {
        this.v2 = v;
    }


    abstract protected String getSign();


    public void detectBrackets() {
        detectBracketsRecursive(priority);

    }

    protected void detectBracketsRecursive(TokenPriority parentPriority) {
        if(parentPriority.priority > priority.priority) {
            bracketsRequired = true;
        }

        if(v1 instanceof BinaryOperation) {
            ((BinaryOperation) v1).detectBracketsRecursive(priority);
        }

        if(v2 instanceof BinaryOperation) {
            ((BinaryOperation) v2).detectBracketsRecursive(priority);
        }
    }

    @Override
    public String toString() {
        String r = v1.toString() + " " + getSign() + " " + v2.toString();

        if(bracketsRequired) {
            r = "(" + r + ")";
        }
        return r;
    }






}
