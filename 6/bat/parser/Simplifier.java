package bat.parser;

import bat.operations.Const;
import bat.operations.OperationInterface;
import bat.operations.Variable;
import bat.operations.binary.Add;
import bat.operations.binary.BinaryOperation;
import bat.operations.binary.Divide;
import bat.operations.binary.Multiply;
import bat.operations.binary.Subtract;


abstract public class Simplifier {

    protected BinaryOperation operation;

    public static class SimplifierAdd extends Simplifier {

        public SimplifierAdd(BinaryOperation operation) {
            super(operation);
        }

        @Override
        public OperationInterface simplifyElement() {
            Add operation = (Add)this.operation;
            OperationInterface oi1, oi2;

            oi1 = operation.getV1();
            oi2 = operation.getV2();

            if(oi1 instanceof Const && oi1.getValue() == 0) {
                return oi2;
            }
            else if(oi2 instanceof Const && oi2.getValue() == 0) {
                return oi1;
            }
            else {
                return operation;
            }
        }
    }

    public static class SimplifierSubtract extends Simplifier {

        public SimplifierSubtract(BinaryOperation operation) {
            super(operation);
        }

        @Override
        public OperationInterface simplifyElement() {
            Subtract operation = (Subtract)this.operation;
            OperationInterface oi1, oi2;

            oi1 = operation.getV1();
            oi2 = operation.getV2();

            if(oi2 instanceof Const && oi2.getValue() == 0) { // anything - 0
                return oi1;
            }
            else if(oi1 instanceof Const && oi2 instanceof Const && oi1.getValue() == oi2.getValue()) { // 9 - 9
                return new Const(0);
            }
            else if(oi1 instanceof Variable
                    && oi2 instanceof Variable
                    && ((Variable)oi1).getVarName().equals( ((Variable)oi2).getVarName() )) { // X - X
                return new Const(0);
            }
            else {
                return operation;
            }
        }
    }

    public static class SimplifierMultiply extends Simplifier {

        public SimplifierMultiply(BinaryOperation operation) {
            super(operation);
        }

        @Override
        public OperationInterface simplifyElement() {
            Multiply operation = (Multiply)this.operation;
            OperationInterface oi1, oi2;

            oi1 = operation.getV1();
            oi2 = operation.getV2();

            if((oi1 instanceof Const && oi1.getValue() == 0)
                    || (oi2 instanceof Const && oi2.getValue() == 0)) { // anything * 0
                return new Const(0);
            }

            if(oi1 instanceof Const && oi1.getValue() == 1) {
                return oi2;
            }
            else if(oi2 instanceof Const && oi2.getValue() == 1) {
                return oi1;
            }
            else {
                return operation;
            }
        }
    }

    public static class SimplifierDivide extends Simplifier {

        public SimplifierDivide(BinaryOperation operation) {
            super(operation);
        }

        @Override
        public OperationInterface simplifyElement() {
            Divide operation = (Divide)this.operation;
            OperationInterface oi1, oi2;

            oi1 = operation.getV1();
            oi2 = operation.getV2();

            if(oi1 instanceof Const && oi1.getValue() == 0) { // anything * 0
                return new Const(0);
            }
            if(oi2 instanceof Const && oi2.getValue() == 1) {
                return oi1;
            }
            else if(oi1 instanceof Variable
                    && oi2 instanceof Variable
                    && ((Variable)oi1).getVarName().equals( ((Variable)oi2).getVarName() )) { // X - X
                return new Const(1);
            }
            else {
                return operation;
            }
        }
    }


    public Simplifier(BinaryOperation operation) {
        this.operation = operation;
    }


    public OperationInterface simplify() throws Exception {
        BinaryOperation o = operation;

        OperationInterface v;

        v = o.getV1();
        if(v instanceof BinaryOperation) {
            Simplifier s = Simplifier.factory((BinaryOperation) v);
            v = s.simplify();
            operation.setV1(v);
        }

        v = o.getV2();
        if(v instanceof BinaryOperation) {
            Simplifier s = Simplifier.factory((BinaryOperation) v);
            v = s.simplify();
            operation.setV2(v);
        }

        Simplifier thisSimplifier = Simplifier.factory(o);
        return thisSimplifier.simplifyElement();
    }


    abstract public OperationInterface simplifyElement();

    public static Simplifier factory(BinaryOperation operation) throws Exception {

        Simplifier r;

        if(operation instanceof bat.operations.binary.Add) {
            r = new SimplifierAdd(operation);
        }
        else if(operation instanceof bat.operations.binary.Subtract) {
            r = new SimplifierSubtract(operation);
        }
        else if(operation instanceof bat.operations.binary.Multiply) {
            r = new SimplifierMultiply(operation);
        }
        else if(operation instanceof bat.operations.binary.Divide) {
            r = new SimplifierDivide(operation);
        }
        else {
            throw new Exception("Wrong BinaryOperation class");
        }

        return r;
    }




















}
