package bat.parser;

import java.util.ArrayList;

import bat.operations.Const;
import bat.operations.OperationInterface;
import bat.operations.Variable;
import bat.operations.binary.Add;
import bat.operations.binary.BinaryOperation;
import bat.operations.binary.Divide;
import bat.operations.binary.Multiply;
import bat.operations.binary.Subtract;
import bat.parser.Lexer.Token;
import bat.parser.Lexer.TokenType;


public class Parser {

    private int pointer = 0;


    private boolean strangeFlag = false;



    public OperationInterface parse(ArrayList<Token> tokenList) throws Exception {
        pointer = 0;
        return parseRecursive(tokenList, 0);
    }

    private BinaryOperation binaryOperationFactory(Token token, OperationInterface param1, OperationInterface param2)
            throws Exception {
        if(token.type == TokenType.ADD) {
            if(token.data.equals("+")) {
                return new Add(param1, param2);
            }
            else if(token.data.equals("-")) {
                return new Subtract(param1, param2);
            }
        }
        else if(token.type == TokenType.MULTIPLY) {
            if(token.data.equals("*")) {
                return new Multiply(param1, param2);
            }
            else if(token.data.equals("/")) {
                return new Divide(param1, param2);
            }
        }

        throw new Exception("Operation factory supports only +-*/");
    }

    private OperationInterface tokenToObject(Token token) throws Exception {
        OperationInterface r;

        if(token.type == TokenType.NUMBER) {
            r = new Const(Integer.valueOf(token.data));
        }
        else if(token.type == TokenType.VARIABLE) {
            r = new Variable(token.data);
        }
        else {
            throw new Exception("Expected number or variable");
        }

        return r;
    }





    private OperationInterface parseRecursive(ArrayList<Token> tokenList, int prevPriority) throws Exception {

        OperationInterface param1, param2;
        Token argToken, signToken;
        int priority;

        argToken = tokenList.get(pointer);

        if(argToken.type == TokenType.BRACKETOPEN) {
            pointer++;
            param1 = parseRecursive(tokenList, 0);
        }
        else {
            param1 = tokenToObject(argToken);
        }

        while(true) {
            // points to the last element?
            if(pointer >= tokenList.size() - 1) {
                return param1;
            }

            if(tokenList.get(pointer + 1).type == TokenType.BRACKETCLOSE) {
                pointer++;
                strangeFlag = true;
                return param1;
            }

            signToken = tokenList.get(pointer + 1);
            priority = signToken.priority;

            if(priority > prevPriority) {
                pointer += 2;
                param2 = parseRecursive(tokenList, priority);
                param1 = binaryOperationFactory(signToken, param1, param2);
                if(strangeFlag) {
                    strangeFlag = false;
                    //pointer++;
                    return param1;
                }
            }
            else {
                // priority of this operation is lower than previous. Lets just return the param1 and
                // return the control to lower-priority tasks
                return param1;
            }
        }









        //Token token = tokenList.get(pointer);














        // look for opening bracket.
        /*
        for(int i = pointer; i < tokenList.size(); i++) {
            if(tokenList.get(i).type == TokenType.BRACKETOPEN) {

            }

        }


        Token token, signToken;
        while(true) {
            token = tokenList.get(pointer);
            if(token.type == Lexer.TokenType.NUMBER) {
                param1 = new Const(Integer.parseInt(token.data));
            }
            else if(token.type == Lexer.TokenType.VARIABLE) {
                param1 = new Variable(token.data);
            }

            pointer++;
            token = tokenList.get(pointer); // sign

            pointer++;






        }
*/


    }




}
