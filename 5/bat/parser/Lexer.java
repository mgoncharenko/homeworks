package bat.parser;

import java.util.ArrayList;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Lexer {
    public static enum TokenType {
        // Token types cannot have underscores
        NUMBER("-?[0-9]+"),
        ADD("[+|-]"),
        MULTIPLY("[*|/]"),
        WHITESPACE("[ \t\f\r\n]+"),
        BRACKETOPEN("\\("),
        BRACKETCLOSE("\\)"),
        VARIABLE("-?[A-Za-z]+");

        public final String pattern;

        private TokenType(String pattern) {
            this.pattern = pattern;
        }
    }

    public static enum TokenPriority {
        // Priorities of operations
        NUMBER(0),
        ADD(1),
        MULTIPLY(2),
        WHITESPACE(0),
        BRACKETOPEN(3),
        BRACKETCLOSE(3),
        VARIABLE(0);

        public final int priority;

        private TokenPriority(int priority) {
            this.priority = priority;
        }
    }


    public static class Token {
        public TokenType type;
        public int priority;
        public String data;

        public Token(TokenType type, String data) {
            this.type = type;
            this.data = data;
            this.priority = TokenPriority.valueOf(type.name()).priority;
        }

        @Override
        public String toString() {
            return String.format("(%s %s)", type.name(), data);
        }
    }


    public static ArrayList<Token> lex(String input) {
        // The tokens to return
        ArrayList<Token> tokens = new ArrayList<Token>();

        // Lexer logic begins here
        StringBuffer tokenPatternsBuffer = new StringBuffer();
        for (TokenType tokenType : TokenType.values())
            tokenPatternsBuffer.append(String.format("|(?<%s>%s)", tokenType.name(), tokenType.pattern));

        String _tmpStr = new String(tokenPatternsBuffer.substring(1));

        Pattern tokenPatterns = Pattern.compile(_tmpStr);

        // Begin matching tokens
        Matcher matcher = tokenPatterns.matcher(input);
        while (matcher.find()) {
            if (matcher.group(TokenType.NUMBER.name()) != null) {
                tokens.add(new Token(TokenType.NUMBER, matcher.group(TokenType.NUMBER.name())));
                continue;
            } else if (matcher.group(TokenType.ADD.name()) != null) {
                tokens.add(new Token(TokenType.ADD, matcher.group(TokenType.ADD.name())));
                continue;
            } else if (matcher.group(TokenType.MULTIPLY.name()) != null) {
                tokens.add(new Token(TokenType.MULTIPLY, matcher.group(TokenType.MULTIPLY.name())));
                continue;
            } else if (matcher.group(TokenType.BRACKETOPEN.name()) != null) {
                tokens.add(new Token(TokenType.BRACKETOPEN, matcher.group(TokenType.BRACKETOPEN.name())));
                continue;
            } else if (matcher.group(TokenType.BRACKETCLOSE.name()) != null) {
                tokens.add(new Token(TokenType.BRACKETCLOSE, matcher.group(TokenType.BRACKETCLOSE.name())));
                continue;
            } else if (matcher.group(TokenType.VARIABLE.name()) != null) {
                tokens.add(new Token(TokenType.VARIABLE, matcher.group(TokenType.VARIABLE.name())));
                continue;
            } else if (matcher.group(TokenType.WHITESPACE.name()) != null) {
                continue;
            }
        }

        return tokens;
    }










}
