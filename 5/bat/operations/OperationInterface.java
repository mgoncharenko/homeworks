package bat.operations;

public interface OperationInterface {
    public OperationInterface assignVar(String varName, int varValue);

    public int getValue();
}
