package bat;

import bat.implementor.ClassImpl;



public class Test {


    public static void main(String[] args) {


        if(args.length < 2) {
            System.out.println("Parameters: <className to generate> <superclass or interface>");
        }
        else {

            String className = args[0];
            String superclassName = args[1];

            try {
                ClassImpl ci;
                ci = ClassImpl.factory(className, Class.forName(superclassName));
                System.out.println(ci.generateCode());
            } catch (ClassNotFoundException e) {
                System.out.println("Class " + superclassName + " not found");
            }

        }


        /*
        Test test = new Test();
        test.main();
        */

    }





}
