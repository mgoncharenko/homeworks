package bat;

import java.util.ArrayList;

public interface AInterfaceDemo {
	
	public void method1(String param1, int param2);		
	abstract public String abstractMethod(String str, AInterfaceDemo obj);	
	int method2(int param1, String param2);
	public boolean method3(ArrayList<Integer> alist);
}
