package bat;

import java.util.ArrayList;

abstract public class AClassDemo {
	
	public void method1(String param1, int param2) {
		
	}
		
	abstract protected Object abstractProtectedObjectProcessor(Object o);
	
	abstract protected Integer abstractProtectedArrayProcessor(Integer[] o);
	
	
	abstract public String abstractMethod(String str, AClassDemo obj);
	
	protected int method2(int param1, String param2) {
		
		return 10;		
	}	
	
	public boolean method3(ArrayList<Integer> alist) {
		return true;
	}

}
