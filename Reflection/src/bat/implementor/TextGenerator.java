package bat.implementor;

public interface TextGenerator {

    /**
     * Generates the code based on object passed in constructor
     * @return String
     */
    public String generateCode();
}