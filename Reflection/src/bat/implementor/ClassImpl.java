package bat.implementor;

import java.lang.reflect.Method;
import java.lang.reflect.Modifier;





abstract public class ClassImpl implements TextGenerator {

	protected Class<?> superclass;
	
    protected String newClassName;
    protected String result = "";
    
    
    
    private static class ClassImplFromSuperclass extends ClassImpl {    	
    	
		public ClassImplFromSuperclass(String newClassName, Class<?> superclass) {
			super(newClassName);
			this.superclass = superclass;			
		}
		
		protected void applySuperclass() {
			result += " extends " + superclass.getCanonicalName();
		}		
		
		
		protected void applyMethods() {
			Method[] methods = superclass.getDeclaredMethods();
			
			for(Method method: methods) {
				if(Modifier.isAbstract(method.getModifiers())) {
					addMethod(method);
				}
			}			
		}		
    }
    
    private static class ClassImplFromInterface extends ClassImpl {
    	
    	public ClassImplFromInterface(String newClassName, Class<?> superclass) {
    		super(newClassName);
			this.superclass = superclass;
		}
		
		protected void applySuperclass() {
			result += " implements " + superclass.getCanonicalName();
		}

		@Override
		protected void applyMethods() {
			Method[] methods = superclass.getDeclaredMethods();
			
			for(Method method: methods) {
				addMethod(method);				
			}			
		}    	
    }
    
    public ClassImpl(String newClassName) {
    	this.newClassName = newClassName;
    }
    
        
    
    public static ClassImpl factory(String newClassName, Class<?> superclassOrInterface) {
    	if(superclassOrInterface.isInterface()) {
    		return new ClassImplFromInterface(newClassName, superclassOrInterface);
    	}
    	else {
    		return new ClassImplFromSuperclass(newClassName, superclassOrInterface);
    	}
    }
    
    abstract protected void applySuperclass();
    
    abstract protected void applyMethods();
   

    
    protected void addMethod(Method m) {
    	
    	String str = "";
    	
    	int mods = m.getModifiers();
    	
    	mods = (mods & ~Modifier.ABSTRACT);    	
    	
    	str += Modifier.toString(mods) + " " + m.getReturnType().getName() + " " + m.getName() + "(";
    	
    	int i = 0;
    	for(Class<?> param: m.getParameterTypes()) {
    		if(i > 0) str += ", ";    		
    		str += param.getCanonicalName() + " arg" + String.valueOf(i);    		
    		i++;    		
    	}
    	
    	str += ") {\n";
    	
    	String returnValue = DefaultsGenerator.getDefaultValue(m.getReturnType());
    	if(returnValue != null) {
    		str += "return " + returnValue  + ";";    		
    	}
    	else {
    		str += "return;";
    	}
    	
    	
    	
    	    	
    	str += "\n}\n";
    	
    	result += str;
    }
    
    


    @Override
    public String generateCode() {

        result = "public class " + newClassName;       
        applySuperclass();
        result += "{\n";
        
        applyMethods();        
        
        result += "\n}\n";
        
                
        return result;
    }

}
