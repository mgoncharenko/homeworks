package bat.implementor;

import java.util.HashMap;

public class DefaultsGenerator {
	
	public static String getDefaultValue(Class<?> returnType) {
		String r;		
		
		
		if(returnType == String.class) {
			r = "\"\"";
		}
		else if(returnType == Boolean.class || returnType == boolean.class) {
			r = "false";
		}
		else if(returnType == Integer.class || returnType == Byte.class || returnType == Character.class 
				|| returnType == int.class || returnType == byte.class || returnType == char.class) {
			r = "0";
		}
		else if(returnType == Double.class || returnType == Float.class 
				|| returnType == double.class || returnType == float.class) {
			r = "0.0";
		}
		else if(returnType == Void.class || returnType == void.class) {
			r = null;
		}
		else {
			r = "null";
		}		
		
		return r;
	}
	
	
	
	

}
