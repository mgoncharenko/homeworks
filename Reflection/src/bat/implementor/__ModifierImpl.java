package bat.implementor;

import java.lang.reflect.Modifier;


public class __ModifierImpl implements TextGenerator {

    int mod;

    public __ModifierImpl(int m) {
        this.mod = m;
    }


    @Override
    public String generateCode() {

        /*
        String r = "";

        if(Modifier.isAbstract(mod)) {
            r += "abstract";
        }

        if(Modifier.isPublic(mod)) {
            r += "public ";
        }
        else if(Modifier.isPrivate(mod)) {
            r += "private ";
        }
        else if(Modifier.isProtected(mod)) {
            r += "protected ";
        }

        if(Modifier.isFinal(mod)) {
            r += "final ";
        }
        */

        return (Modifier.toString(mod));
    }
}
