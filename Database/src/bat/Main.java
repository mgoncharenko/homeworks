package bat;

import java.sql.Connection;
import java.sql.Driver;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Properties;

import bat.csv.Country;
import bat.csv.Reader;
import bat.db.AppIds;
import bat.db.Countries;

public class Main {

    private final String serverName = "localhost";
    private final int portNumber = 3306;
    private final String dbName = "java";
    private final String userName = "root";
    private final String password = "root";

    public Connection getConnection() throws SQLException, ClassNotFoundException {

        Connection conn;
        String jdbcUrl = "jdbc:mysql://" + this.serverName + ":" + this.portNumber + "/" + this.dbName;

        Properties connectionProps = new Properties();
        connectionProps.put("user", this.userName);
        connectionProps.put("password", this.password);

        ArrayList<Driver> driversList = Collections.list(DriverManager.getDrivers());
        for(Driver driver: driversList) {
            log(driver.toString());
        }

        //Class.forName("org.gjt.mm.mysql.Driver");
        conn = DriverManager.getConnection(jdbcUrl, connectionProps);

        log("Connected to database " + jdbcUrl);
        return conn;
    }

    private void _main(String[] args) throws Exception {


        int count = 0;
        Country element;

        Connection conn = this.getConnection();

        Countries countriesTable = new Countries(conn);
        countriesTable.create();


        try(Reader r = new Reader("D:\\java\\Database\\data\\allCountries.txt")) {
            r.open();

            while(r.hasNext() && count < 40) {
                element = r.next();
                element.toTable(countriesTable);

                log(element.get(2));
                count++;
            }
        }


        if(count>0) return;











        conn = this.getConnection();
        AppIds table = new AppIds(conn);

        table.create();

        java.sql.Statement stmt = null;
        try {
            stmt = conn.createStatement();
            ResultSet rs = stmt.executeQuery("select * from app_ids");

            while (rs.next()) {
                System.out.println("GUID=" + rs.getString("guid") + " app_id=" + rs.getString("appIdentifier"));
            }
        }
        catch(SQLException e) {
            e.printStackTrace();
        }
        finally {
            if(stmt != null) {
                log("Closed");
                stmt.close();
            }
        }



        log("Yo world");

        table.drop();
        conn.close();
    }




    private void open() {
        // TODO Auto-generated method stub

    }

    public static void log(String str) {
        System.out.println(str);
    }

    public static void main(String[] args) throws Exception {
        Main m = new Main();
        m._main(args);
    }




}
