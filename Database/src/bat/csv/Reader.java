package bat.csv;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Scanner;
import java.util.regex.Pattern;

public class Reader implements AutoCloseable {

    private String fileName;
    private Scanner scanner;

    private final int FIELDS = 12;

    private Country next;


    public Reader(String fileName) {
        this.fileName = fileName;
    }

    public void open() throws FileNotFoundException {
        File file = new File(fileName);
        scanner = new Scanner(file);
        scanner.useDelimiter(Pattern.compile("[\\t\\n]"));
    }

    public Boolean hasNext() {

        int i;
        next = new Country(FIELDS);

        for(i = 0; i < FIELDS; i++) {
            if(scanner.hasNext()) {
                next.add(scanner.next());
            }
            else {
                return false;
            }
        }

        return true;
    }

    public Country next() throws IOException {
        Country r = next;

        if(next == null) {
            throw new IOException("Must call hasNext() before");
        }

        next = null;
        return r;
    }

    @Override
    public void close() throws Exception {
        scanner.close();
    }



}
