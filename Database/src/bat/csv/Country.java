package bat.csv;

import java.sql.SQLException;
import java.util.ArrayList;

import bat.db.Countries;

public class Country extends ArrayList<String> {

    private static final long serialVersionUID = 7876768991442167582L;

    private final int C_CODE       = 0;
    private final int C_POSTAL     = 1;
    private final int C_PLACE      = 2;
    private final int C_LAT        = 9;
    private final int C_LON        = 10;
    private final int C_ACCURACY   = 11;


    public Country(int capacity) {
        super(capacity);
    }


    public void toTable(Countries table) throws NumberFormatException, SQLException {

        table.insert(get(C_CODE), get(C_POSTAL), get(C_PLACE),
                Double.parseDouble(get(C_LAT)), Double.parseDouble(get(C_LON)),
                Integer.parseInt(get(C_ACCURACY)));
    }

}
