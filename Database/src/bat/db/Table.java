package bat.db;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

abstract public class Table {
    protected Connection connection;
    protected String name;
    protected String createQuery;
    protected String dropQuery;

    public Table(Connection conn) {
        connection = conn;
    }

    public Connection getConnection() {
        return this.connection;
    }

    public void create() throws SQLException {
        Statement stmt = getConnection().createStatement();
        stmt.execute(this.createQuery);
    }

    public void drop() throws SQLException {
        Statement stmt = getConnection().createStatement();
        stmt.execute(this.dropQuery);
    }

    public ResultSet fetchAll() throws SQLException {
        Statement stmt = getConnection().createStatement();
        return stmt.executeQuery("select * from " + this.name);
    }


}
