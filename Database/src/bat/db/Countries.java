package bat.db;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;



public class Countries extends Table {

    public final String C_ID          = "id";
    public final String C_CODE        = "code";
    public final String C_POSTAL      = "postal";
    public final String C_PLACE       = "place";
    public final String C_LAT         = "lat";
    public final String C_LON         = "lon";
    public final String C_ACCURACY    = "accuracy";

    public Countries(Connection conn) {
        super(conn);

        name = "countries";

        createQuery = "CREATE TABLE `" + this.name + "` ("
                + "`id` bigint(20) NOT NULL AUTO_INCREMENT,"
                + "`code` varchar(4) NOT NULL,"
                + "`postal` varchar(32) NOT NULL,"
                + "`place` varchar(128) NOT NULL,"
                + "`lat` DOUBLE NOT NULL,"
                + "`lon` DOUBLE NOT NULL,"
                + "`accuracy` TINYINT NOT NULL,"
                + "PRIMARY KEY (`id`)"
                + ") ENGINE=InnoDB DEFAULT CHARSET=utf8";

        dropQuery = "DROP TABLE `" + this.name + "`";
    }

    public void insert(String code, String postal, String place, double lat, double lon, int accuracy)
            throws SQLException {
        Connection conn = getConnection();

        //PreparedStatement stmt = conn.prepareStatement("INSERT INTO " + name + " VALUES(null, :code, :postal, :place, :lat, :lon, :accuracy)");
        PreparedStatement stmt = conn.prepareStatement("INSERT INTO " + name + " VALUES(null, ?, ?, ?, ?, ?, ?)");

        stmt.setString(1,  code);
        stmt.setString(2,  postal);
        stmt.setString(3,  place);
        stmt.setDouble(4,  lat);
        stmt.setDouble(5,  lon);
        stmt.setInt(6,  accuracy);

        stmt.execute();
    }


}
