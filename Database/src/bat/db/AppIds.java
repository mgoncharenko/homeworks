package bat.db;

import java.sql.Connection;

public class AppIds extends Table {

    public final String C_ID          = "id";
    public final String C_NAME        = "name";
    public final String C_VALUE       = "value";
    public final String C_DESCRIPTION = "description";

    public AppIds(Connection conn) {
        super(conn);

        name = "app_ids";

        createQuery = "CREATE TABLE `" + this.name + "` ("
                + "`id` bigint(20) NOT NULL AUTO_INCREMENT,"
                + "`name` varchar(128) NOT NULL,"
                + "`value` varchar(128) NOT NULL,"
                + "`description` longtext NOT NULL,"
                + "PRIMARY KEY (`id`)"
                + ") ENGINE=InnoDB DEFAULT CHARSET=utf8";

        dropQuery = "DROP TABLE `" + this.name + "`";
    }

}
