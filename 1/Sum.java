
public class Sum {
	
	private static int sumArray(String[] a) {
		int i;
		int sum = 0;
		
		for(i = 0; i < a.length; i++) {
			a[i] = a[i].trim();
			try {
				sum += Integer.parseInt(a[i]);
			}
			catch(NumberFormatException e) {
				sum += sumArray(a[i].split("\\s+"));
			}
		}
		
		return sum;		
	}	
	
	public static void main(String[] args) {
		
		int sum = 0;
		
		if(args.length == 0) {
			System.out.println("No args");
		}
		else {			
			sum = sumArray(args);			
			System.out.println("Sum = " + sum);
		}
	}
}
