
public class Test {
	
	
	private static class C1 {
		{
			System.out.println("C1 init");
		}
		
		public C1(int x) {
			System.out.println("C1 custom constructor");
		}
		
	}
	
	
	private static class C2 extends C1 {
		{
			System.out.println("C2 init");
		}
		
		public C2() {
			super(10);
			System.out.println("C2 constructor");
		}
	}
	
	
	public static void main(String[] args) {
		
		System.out.println("Main starting");
		
		C2 c2 = new C2();
	}
	
	
	
	
	
	
}
