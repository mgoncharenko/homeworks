
public class BinarySearch {
	
	abstract public class SearchAbstract {		
		protected String identifier;
				
		public String getIdentifier() {
			return identifier;			
		}
		
		abstract public int search(int v, int[] a);		
	}
	
	public class SearchIteration extends SearchAbstract {
		
		public SearchIteration() {
			identifier = "Iteration";
		}

		public int search(int x, int[] a) {
			
			int first = 0;
			int last  = a.length - 1;			
			int mid   = -1;
			
			while(true) {
				
				mid = first + (last - first) / 2;
				
				if(a[first] <= x) {
					return first;
				}
				
				else if(a[last] <= x) {
					
					if(last - first <= 1) {
						return last;
					}
					
					if(a[mid] > x) {
						first = mid;
					}
					else if(a[mid] <= x) {
						last = mid;
					}
				}
				else {
					return -1;
				}				
			}
		}
	}
	
	public class SearchRecursive extends SearchAbstract {
		
		public SearchRecursive() {
			identifier = "Recursive";
		}
		
		private int doRecursiveSearch(int x, int[] a, int first, int last) {
			
			int mid = first + (last - first) / 2;

			if(last - first <= 1) {
				return last;
			}
			
			if(a[first] <= x) {
				return first;
			}
			else if(a[last] > x) {
				return -1;
			}
			
			if(a[mid] > x) {
				return doRecursiveSearch(x, a, mid, last);
			}
			else { //if(a[mid] <= x) {
				return doRecursiveSearch(x, a, first, mid);
			}			
		}		

		public int search(int x, int[] a) {
			return doRecursiveSearch(x, a, 0, a.length - 1);
		}
	}
	
	private static void printArray(int[] array) {		
		System.out.println("---------------");
		
		for(int i = 0; i < array.length; i++) {
			System.out.println(i + " = " + array[i]);
		}	
		System.out.println("---------------");
	}
	
	
	private void _main(String[] args) {
		
		int[] a;
		int x;
		int i;
		int r;
		SearchAbstract sa;
		
		if(args.length < 2) {
			System.out.println("Specify <v> <x1> <x2> ....");
		}
		else {
			// minimum value to look for
			x = Integer.parseInt(args[0]);
			
			// fill integer array
			a = new int[args.length - 1];
			for(i = 1; i < args.length; i++) {
				a[i - 1] = Integer.parseInt(args[i]);
			}
			
			printArray(a);			
						
			sa = new SearchIteration();
			r = sa.search(x, a);
			System.out.println(sa.getIdentifier() + ": Minimum index where a[i] <= " + x + ": " + r);
			
			sa = new SearchRecursive();
			r = sa.search(x, a);
			System.out.println(sa.getIdentifier() + ": Minimum index where a[i] <= " + x + ": " + r);			
			
		}		
	}
	
	public static void main(String[] args) {
		
		BinarySearch bs = new BinarySearch();
		bs._main(args);
	}
}
 